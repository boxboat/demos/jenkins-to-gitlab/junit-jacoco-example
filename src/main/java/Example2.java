/**
 *
**/
public class Example2 {
	public int mod5(int input) {
		return input % 5;
	}

	public int add(int a, int b) {
		return a + b;
	}

	public int sub(int a, int b) {
		return a - b;
	}

	public int greater(int a, int b) {
		return (a >= b) ? a : b;
	}

	public boolean equal(int a, int b) {
		return (a == b);
	}
}
