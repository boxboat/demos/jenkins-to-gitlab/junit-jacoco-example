import org.junit.Test;
import static org.junit.Assert.*;

public class Example2Test {
	@Test
	public void verifyAdd() {
		Example2 e2 = new Example2();
		int res = e2.add(5, 2);
		assertEquals(7, res);
	}

	@Test
	public void verifyGreater() {
		Example2 e2 = new Example2();
		int res = e2.greater(5, 2);
		assertEquals(5, res);
	}
}
